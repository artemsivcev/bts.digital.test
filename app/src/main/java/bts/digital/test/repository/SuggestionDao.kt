package bts.digital.test.repository

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.database.Cursor
import bts.digital.test.api.model.Suggestion

@Dao
interface SuggestionDao {

    @Query("SELECT * from suggestions")
    fun getAll(): Cursor

    @Query("SELECT * from suggestions WHERE text = :text")
    fun getByText(text: String?): Suggestion

    @Insert(onConflict = REPLACE)
    fun insert(suggestion: Suggestion)

    @Query("DELETE from suggestions")
    fun deleteAll()

    @Query("SELECT * from suggestions WHERE text LIKE '%' || :search  || '%'")
    fun getSuggestionLike(search: String?): Cursor
}