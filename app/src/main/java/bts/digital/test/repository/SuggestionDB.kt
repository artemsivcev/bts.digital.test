package bts.digital.test.repository

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import bts.digital.test.api.model.Suggestion

@Database(entities = arrayOf(Suggestion::class), version = 1)
abstract class SuggestionDB : RoomDatabase() {

    abstract fun suggestionDataDao(): SuggestionDao

    companion object {
        private var INSTANCE: SuggestionDB? = null

        fun getInstance(context: Context): SuggestionDB? {
            if (INSTANCE == null) {
                synchronized(SuggestionDB::class) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SuggestionDB::class.java, "suggestions.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}