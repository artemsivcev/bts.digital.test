package bts.digital.test.repository

import io.reactivex.Observable
import bts.digital.test.api.ApiService
import bts.digital.test.api.model.FlickrMain
import bts.digital.test.api.model.FlickrPhotoInfo
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(private val apiService: ApiService) {

    fun getPhotosListFromApi(searchWord : String, page: Long): Observable<FlickrMain> = apiService.searchPhotosByText(searchWord, page)
    fun getPhotoInfoFromApi(photoId : String): Observable<FlickrPhotoInfo> = apiService.getPhotoInfo(photoId)


}