package bts.digital.test.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import bts.digital.test.ui.main.FlickrInfoActivity
import bts.digital.test.ui.main.FlickrSearchActivity
import bts.digital.test.ui.main.di.FlickerSearchModule
import bts.digital.test.ui.main.di.FlickrInfoModule

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = arrayOf(FlickerSearchModule::class))
    abstract fun bindFlickerSearchModule(): FlickrSearchActivity

    @ContributesAndroidInjector(modules = arrayOf(FlickrInfoModule::class))
    abstract fun bindFlickrInfoActivity(): FlickrInfoActivity
}