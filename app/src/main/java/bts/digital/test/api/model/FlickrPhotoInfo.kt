package bts.digital.test.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
data class FlickrPhotoInfo(
        @SerializedName("photo") @Expose val photo: FlickrPhoto,
        @SerializedName("stat") @Expose var stat: String
)