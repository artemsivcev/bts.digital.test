package bts.digital.test.api.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "suggestions")
data class Suggestion(
        @PrimaryKey(autoGenerate = true) var _id: Long?,
        @ColumnInfo(name = "text") var text: String
){
    constructor():this(null,"")
}