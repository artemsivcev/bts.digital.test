package bts.digital.test.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName
import bts.digital.test.commons.adapter.ViewType
import bts.digital.test.commons.constants.AdapterConstants

@JsonIgnoreProperties(ignoreUnknown = true)
data class FlickrPhoto(
        @SerializedName("_id") val photoId: String,
        @SerializedName("owner") val ownerId: String,
        @SerializedName("farm") val farmId: Long,
        @SerializedName("server") val serverId: String,
        @SerializedName("secret") val secret: String,
        @SerializedName("title") val title: String,
        @SerializedName("isPublic") val isPublic: Long,
        @SerializedName("isFriend") val isFriend: Long,
        @SerializedName("isFamily") val isFamily: Long,

        @SerializedName("url_m") var url_m: String? = null,
        @SerializedName("height_m") var height_m: String? = null,
        @SerializedName("width_m") var width_m: String? = null,
        @SerializedName("url_t") var url_t: String? = null,
        @SerializedName("height_t") var height_t: Int? = null,
        @SerializedName("width_t") var width_t: Int? = null,
        @SerializedName("url_o") var url_o: String? = null,
        @SerializedName("height_o") var height_o: String? = null,
        @SerializedName("width_o") var width_o: String? = null
) : ViewType {

    override fun getViewType() = AdapterConstants.FLICKR

}