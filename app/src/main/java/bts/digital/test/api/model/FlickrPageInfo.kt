package bts.digital.test.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
data class FlickrPageInfo(
        @SerializedName("page") val page: Long,
        @SerializedName("pages") val pages: Long,
        @SerializedName("perpage") val perpage: Long,
        @SerializedName("total") val total: String,
        @SerializedName("photo") @Expose val photo: List<FlickrPhoto>
)

