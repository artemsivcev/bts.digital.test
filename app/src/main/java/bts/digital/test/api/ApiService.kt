package bts.digital.test.api

import retrofit2.http.GET
import retrofit2.http.Query
import io.reactivex.Observable
import bts.digital.test.api.model.FlickrMain
import bts.digital.test.api.model.FlickrPhotoInfo
import bts.digital.test.commons.constants.Constants.FLICKR_API_KEY

interface ApiService {

    @GET("?method=flickr.photos.search&api_key="+FLICKR_API_KEY+"&format=json&nojsoncallback=1&extras=url_m,url_t,url_o")
    fun searchPhotosByText(@Query("text") text: String, @Query("page") page: Long): Observable<FlickrMain>

    @GET("?method=flickr.photos.getInfo&api_key="+FLICKR_API_KEY+"&format=json&nojsoncallback=1&extras=url_m,url_t,url_o")
    fun getPhotoInfo(@Query("photo_id") photoId: String): Observable<FlickrPhotoInfo>


}