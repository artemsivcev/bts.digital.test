package bts.digital.test.api.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
data class FlickrMain(
        @SerializedName("photos") val photos: FlickrPageInfo,
        @SerializedName("stat") val stat: String
)
