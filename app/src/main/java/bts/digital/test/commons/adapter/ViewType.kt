package bts.digital.test.commons.adapter

interface ViewType {
    fun getViewType(): Int
}