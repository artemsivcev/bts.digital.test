package bts.digital.test.ui.main.di

import dagger.Module
import dagger.Provides
import bts.digital.test.repository.Repository
import bts.digital.test.ui.main.FlickrSearchViewModel
import bts.digital.test.util.SchedulerProvider

@Module
class FlickerSearchModule {

    @Provides
    fun provideViewModel(repository: Repository, schedulerProvider: SchedulerProvider) = FlickrSearchViewModel(repository, schedulerProvider)
}