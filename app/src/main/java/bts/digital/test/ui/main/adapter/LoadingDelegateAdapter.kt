package bts.digital.test.ui.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import bts.digital.test.R
import bts.digital.test.commons.adapter.ViewType
import bts.digital.test.commons.adapter.ViewTypeDelegateAdapter
import bts.digital.test.commons.extensions.inflate

class LoadingDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup) = LoadingViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
    }

    class LoadingViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.item_loading))
}