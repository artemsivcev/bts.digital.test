package bts.digital.test.ui.main

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.os.*
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.droidcba.kedditbysteps.commons.ScrollListener
import com.facebook.drawee.backends.pipeline.Fresco
import dagger.android.DaggerActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

import bts.digital.test.api.model.FlickrPhoto
import bts.digital.test.ui.main.adapter.FlickrDelegateAdapter
import bts.digital.test.ui.main.adapter.RecyclerViewAdapter
import java.util.ArrayList
import javax.inject.Inject
import android.support.v7.widget.GridLayoutManager
import android.widget.SearchView
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import bts.digital.test.R
import bts.digital.test.api.model.Suggestion
import bts.digital.test.commons.constants.Constants.EXTRA_PHOTO_TITLE
import bts.digital.test.commons.constants.Constants.EXTRA_URL
import bts.digital.test.commons.constants.Constants.SUGGESTION_FIELD
import bts.digital.test.commons.extensions.androidLazy
import bts.digital.test.repository.SuggestionDB
import bts.digital.test.util.DbWorkerThread


class FlickrSearchActivity() : DaggerActivity(), Parcelable, FlickrDelegateAdapter.onViewSelectedListener, SearchView.OnQueryTextListener, SearchView.OnSuggestionListener {

    private val mCompositeDisposable by lazy { CompositeDisposable() }
    private val mRecyclerViewAdapter by androidLazy { RecyclerViewAdapter(this) }
    private var mCursorAdapter: SimpleCursorAdapter? = null

    @Inject lateinit var mFlickrSearchViewModel: FlickrSearchViewModel

    private var mSearchView: SearchView? = null
    private var mRecyclerView: RecyclerView? = null

    private val mFlickrPhotos = ArrayList<FlickrPhoto>()
    private var mCurrentPage = 1
    private var mSearchTerm: String? = ""
    private var mQueryChange: Boolean? = false

    private var mDb: SuggestionDB? = null

    private lateinit var mDbWorkerThread: DbWorkerThread
    private val mUiHandler = Handler()

    private val TAG = "FlickrSearchActivity"


    constructor(parcel: Parcel) : this() {
        mCurrentPage = parcel.readInt()
        mSearchTerm = parcel.readString()
    }

    /**
     * There is some simple program with Dagger, Rx, Room.
     * I really try to use butterknife but it didnt want work.
     * All data from api and from DB loading in threads.
     * To show images used Fresco (that was really bad chose)
     *
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        Fresco.initialize(this)

        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()

        mDb = SuggestionDB.getInstance(this)

        mSearchView = findViewById(R.id.searchView)
        val searchManager:SearchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager;
        mSearchView?.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        mSearchView?.setOnQueryTextListener(this)
        mSearchView?.setOnSuggestionListener(this)
        mSearchView?.setOnSearchClickListener { showThreeLastSuggestion() }

        mRecyclerView = findViewById(R.id.recyclerView)
        mRecyclerView?.apply {
            setHasFixedSize(true)
            val gridLayout = GridLayoutManager(context, 2)
            layoutManager = gridLayout
            clearOnScrollListeners()
            addOnScrollListener(ScrollListener(
                    {
                        mCurrentPage++
                        loadFlickrPhotos()
                    }, gridLayout))
        }
        mRecyclerView?.adapter = mRecyclerViewAdapter

    }

    //RecyclerView stuff

    override fun onItemSelected(photo: FlickrPhoto?) {
        val intent = Intent(this, FlickrInfoActivity::class.java)
        intent.putExtra(EXTRA_PHOTO_TITLE, photo!!.title)
        //cat photo url from good quality to bad
        var url: String = ""
        if(photo.url_o != null){
            url = photo.url_o!!
        }else if(photo.url_m != null){
            url = photo.url_m!!
        }else if(photo.url_t != null){
            url = photo.url_t!!
        }
        intent.putExtra(EXTRA_URL, url)
        startActivity(intent)
    }

    private fun loadFlickrPhotos() {
        if (mSearchTerm != null) {
            mCompositeDisposable.add(mFlickrSearchViewModel.showListOfPhoto(mSearchTerm!!, mCurrentPage.toLong())
                    .subscribeBy(
                            onComplete = {
                            },
                            onNext = {
                                if(it.photos.photo.size == 0){
                                    showError(getString(R.string.error_nothing_to_show))
                                    return@subscribeBy
                                }
                                updateItemsInList(it.photos.photo)
                                mQueryChange = false
                            },
                            onError = {
                                showError(getString(R.string.error_default))
                                Log.d(TAG, it.message)
                            }
                    ))
        }
    }

    fun showError(error:String){
        mUiHandler.post {
            Toast.makeText(this, error, Toast.LENGTH_LONG).show()
        }
    }

    private fun updateItemsInList(searchResult: List<FlickrPhoto>) {
        mFlickrPhotos.addAll(searchResult)
        mRecyclerView?.adapter?.notifyDataSetChanged()
        if(mQueryChange!!)
            mRecyclerViewAdapter.clearAndAddNewItems(searchResult)
        else
            mRecyclerViewAdapter.addItems(searchResult)
    }

    private fun resetStates() {
        mFlickrPhotos.clear()
        mCurrentPage = 1

        if (mSearchView != null) {
            mSearchView!!.clearFocus()
        }
    }

    //SearchView implementation

    override fun onQueryTextChange(searchText: String): Boolean {
        // here find suggestions
        findSuggestionsInDb(searchText)
        return true
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        resetStates()
        mQueryChange = true
        mSearchTerm = query
        insertSuggestionInDb(query)
        loadFlickrPhotos()
        return true
    }

    override fun onSuggestionClick(position: Int): Boolean {
        var cursor = mSearchView?.getSuggestionsAdapter()?.getItem(position) as Cursor;
        var indexColumnSuggestion = cursor.getColumnIndex( SUGGESTION_FIELD);
        mSearchView?.setQuery(cursor.getString(indexColumnSuggestion), false);

        return true
    }

    override fun onSuggestionSelect(position: Int): Boolean {
        //nothing
        return true
    }

    //Working with room here

    fun showThreeLastSuggestion(){
        val task = Runnable {
            val cursor = mDb?.suggestionDataDao()?.getAll() as Cursor

            mUiHandler.post {
                if (cursor.getCount() != 0) {
                    val columns = arrayOf(SUGGESTION_FIELD)
                    val columnTextId = intArrayOf(android.R.id.text1)

                    mCursorAdapter = SimpleCursorAdapter(getBaseContext(),
                            android.R.layout.simple_list_item_1, cursor,
                            columns, columnTextId, 0);

                    mSearchView?.setSuggestionsAdapter(mCursorAdapter)
                    mCursorAdapter?.notifyDataSetChanged()
                }
            }
        }
        mDbWorkerThread.postTask(task)
    }

    fun insertSuggestionInDb(query:String){
        val task = Runnable { mDb?.suggestionDataDao()?.insert(Suggestion(null,query))}
        mDbWorkerThread.postTask(task)
    }

    fun findSuggestionsInDb(searchText: String){
        val task = Runnable {
            val cursor = mDb?.suggestionDataDao()?.getSuggestionLike(searchText) as Cursor

            mUiHandler.post {
                if (cursor.getCount() != 0) {

                    val columns = arrayOf(SUGGESTION_FIELD)
                    val columnTextId = intArrayOf(android.R.id.text1)

                    mCursorAdapter = SimpleCursorAdapter(getBaseContext(),
                            android.R.layout.simple_list_item_1, cursor,
                            columns, columnTextId, 0)
                    mSearchView?.setSuggestionsAdapter(mCursorAdapter)
                }
            }
        }
        mDbWorkerThread.postTask(task)
    }

    //Activity stuff

    override fun onResume() {
        super.onResume()
        if (mSearchView != null) {
            mSearchView!!.clearFocus()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mDbWorkerThread.quit()
        mCompositeDisposable.clear()
        mCompositeDisposable.dispose()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(mCurrentPage)
        parcel.writeString(mSearchTerm)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FlickrSearchActivity> {
        override fun createFromParcel(parcel: Parcel): FlickrSearchActivity {
            return FlickrSearchActivity(parcel)
        }

        override fun newArray(size: Int): Array<FlickrSearchActivity?> {
            return arrayOfNulls(size)
        }
    }



}
