package bts.digital.test.ui.main

import io.reactivex.Observable
import bts.digital.test.api.model.FlickrPhotoInfo
import bts.digital.test.repository.Repository
import bts.digital.test.util.SchedulerProvider

class FlickrInfoViewModel(private val repository: Repository, private val schedulerProvider: SchedulerProvider) {

    fun showPhotoInfo(photoId : String): Observable<FlickrPhotoInfo> = repository.getPhotoInfoFromApi(photoId)
            .compose(schedulerProvider.getSchedulersForObservable<FlickrPhotoInfo>())

}