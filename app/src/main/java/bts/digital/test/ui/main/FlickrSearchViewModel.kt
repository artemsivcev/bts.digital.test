package bts.digital.test.ui.main

import io.reactivex.Observable
import bts.digital.test.api.model.FlickrMain
import bts.digital.test.repository.Repository
import bts.digital.test.util.SchedulerProvider

class FlickrSearchViewModel(private val repository: Repository, private val schedulerProvider: SchedulerProvider) {

    fun showListOfPhoto(searchWord : String, page: Long): Observable<FlickrMain> = repository.getPhotosListFromApi(searchWord, page)
            .compose(schedulerProvider.getSchedulersForObservable<FlickrMain>())
}