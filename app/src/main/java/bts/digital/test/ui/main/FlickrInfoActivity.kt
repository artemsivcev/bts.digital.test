package bts.digital.test.ui.main

import android.net.Uri
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.widget.TextView
import com.facebook.drawee.view.SimpleDraweeView
import dagger.android.DaggerActivity

import bts.digital.test.R
import bts.digital.test.commons.constants.Constants.EXTRA_PHOTO_TITLE
import bts.digital.test.commons.constants.Constants.EXTRA_URL

class FlickrInfoActivity() : DaggerActivity(), Parcelable {

    private var mImgFlickerFull: SimpleDraweeView? = null

    private var mTvTitle: TextView? = null

    private var mPhotoId: String? = ""
    private var mUrl: String? = ""

    constructor(parcel: Parcel) : this() {
        mPhotoId = parcel.readString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        //ButterKnife didnt work!! Do like old way
//        ButterKnife.bind(this);

        mImgFlickerFull = findViewById(R.id.imgFlicker)
        mTvTitle = findViewById(R.id.tvTitle)

        mPhotoId = intent.getStringExtra(EXTRA_PHOTO_TITLE);
        mUrl = intent.getStringExtra(EXTRA_URL)
        updateUI(mPhotoId!!, mUrl!!)

    }

    private fun loadThumbnail(imageView: SimpleDraweeView, url: String) {
        val uri = Uri.parse(url)
        imageView.setImageURI(uri)
    }

    private fun updateUI(title: String, url: String) {
        mTvTitle!!.text = title
        loadThumbnail(mImgFlickerFull!!, url)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(mPhotoId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FlickrSearchActivity> {
        override fun createFromParcel(parcel: Parcel): FlickrSearchActivity {
            return FlickrSearchActivity(parcel)
        }

        override fun newArray(size: Int): Array<FlickrSearchActivity?> {
            return arrayOfNulls(size)
        }
    }
}
