package bts.digital.test.ui.main.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import bts.digital.test.api.model.FlickrPhoto
import bts.digital.test.commons.adapter.ViewType
import bts.digital.test.commons.adapter.ViewTypeDelegateAdapter
import bts.digital.test.commons.constants.AdapterConstants
import java.util.ArrayList


class RecyclerViewAdapter(listener: FlickrDelegateAdapter.onViewSelectedListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: ArrayList<ViewType>
    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>()
    private val loadingItem = object : ViewType {
        override fun getViewType() = AdapterConstants.LOADING
    }

    init {
        delegateAdapters.put(AdapterConstants.LOADING, LoadingDelegateAdapter())
        delegateAdapters.put(AdapterConstants.FLICKR, FlickrDelegateAdapter(listener))
        items = ArrayList()
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            delegateAdapters.get(viewType).onCreateViewHolder(parent)


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items[position])
    }

    override fun getItemViewType(position: Int) = items[position].getViewType()

    fun addItems(photo: List<FlickrPhoto>) {
        // first remove loading and notify
        val initPosition = items.size - 1
        items.removeAt(initPosition)
        notifyItemRemoved(initPosition)

        // insert news and the loading at the end of the list
        items.addAll(photo)
        items.add(loadingItem)
        notifyItemRangeChanged(initPosition, items.size + 1 /* plus loading item */)
    }

    fun clearAndAddNewItems(photo: List<FlickrPhoto>) {
        items.clear()
        notifyItemRangeRemoved(0, getLastPosition())

        items.addAll(photo)
        items.add(loadingItem)
        notifyItemRangeInserted(0, items.size)
    }

    fun clear() {
        items.clear()
        notifyItemRangeRemoved(0, getLastPosition())
    }

    fun getItems(): List<FlickrPhoto> =
            items
                    .filter { it.getViewType() == AdapterConstants.FLICKR }
                    .map { it as FlickrPhoto }


    private fun getLastPosition() = if (items.lastIndex == -1) 0 else items.lastIndex

}