package bts.digital.test.ui.main.di

import dagger.Module
import dagger.Provides
import bts.digital.test.repository.Repository
import bts.digital.test.ui.main.FlickrInfoViewModel
import bts.digital.test.util.SchedulerProvider

@Module
class FlickrInfoModule {

    @Provides
    fun provideViewModel(repository: Repository, schedulerProvider: SchedulerProvider) = FlickrInfoViewModel(repository, schedulerProvider)
}