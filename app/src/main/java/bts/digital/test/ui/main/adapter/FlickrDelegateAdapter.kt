package bts.digital.test.ui.main.adapter

import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.flickr_entry.view.*
import bts.digital.test.R
import bts.digital.test.api.model.FlickrPhoto
import bts.digital.test.commons.adapter.ViewType
import bts.digital.test.commons.adapter.ViewTypeDelegateAdapter
import bts.digital.test.commons.extensions.inflate

class FlickrDelegateAdapter(val viewActions: onViewSelectedListener) : ViewTypeDelegateAdapter {

    interface onViewSelectedListener {
        fun onItemSelected(photo: FlickrPhoto?)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return FlickrViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as FlickrViewHolder
        holder.bind(item as FlickrPhoto)
    }

    inner class FlickrViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.flickr_entry)) {

        private val imgThumbnail = itemView.imgFlicker
        private val tvTitle = itemView.tvTitle

        fun bind(item: FlickrPhoto) {
            loadThumbnail(imgThumbnail, item.url_t!!)
            tvTitle.text = item.title

            super.itemView.setOnClickListener { viewActions.onItemSelected(item)}
        }
    }

    private fun loadThumbnail(imageView: SimpleDraweeView, url: String) {
        val uri = Uri.parse(url)
        imageView.setImageURI(uri)
    }
}